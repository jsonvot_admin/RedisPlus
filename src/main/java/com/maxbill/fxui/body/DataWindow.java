package com.maxbill.fxui.body;

import com.maxbill.base.bean.Connect;
import com.maxbill.base.bean.DataNode;
import com.maxbill.base.bean.TreeNode;
import com.maxbill.fxui.foot.FootWindow;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

import java.util.List;

import static com.maxbill.fxui.body.BodyWindow.getTabPane;
import static com.maxbill.fxui.util.CommonConstant.*;
import static com.maxbill.fxui.util.CommonFxuiUtil.getNodeIcon;
import static com.maxbill.util.RedisUtil.*;
import static com.maxbill.util.StringUtil.isNull;

/**
 * 数据窗口
 *
 * @author MaxBill
 * @date 2019/07/10
 */
public class DataWindow {

    private static Tab dataTab = null;

    private static Pane dataInfo;

    /**
     * 初始化数据视图
     *
     * @param rootName 数据节点
     * @param index    数据库索引
     */
    public static void initDataTab(String rootName, int index) {
        if (null == dataTab) {
            dataTab = new Tab(TEXT_TAB_DATA);
            getTabPane().getTabs().add(dataTab);
        }
        getTabPane().getSelectionModel().select(dataTab);
        HBox dataPane = new HBox();

        TreeView<TreeNode> dataTree = new TreeView<>();
        dataTree.setShowRoot(true);
        dataTree.setMinWidth(300);
        dataTree.setMaxWidth(350);
        dataTree.setMinHeight(427);
        dataTree.setId("data-tree");
        dataTree.setFixedCellSize(30);

        TreeItem<TreeNode> rootTree = new TreeItem<>();
        rootTree.setExpanded(true);
        rootTree.setValue(new TreeNode(rootName));
        rootTree.setGraphic(getNodeIcon(BODY_TREE_01));

        // 获取数据列表
        List<TreeNode> nodeList = getDataList(index, 1000, null);
        if (null != nodeList && !nodeList.isEmpty()) {
            for (TreeNode treeNode : nodeList) {
                TreeItem<TreeNode> treeItem = new TreeItem<>(treeNode);
                treeItem.setGraphic(getNodeIcon(BODY_TREE_02));
                rootTree.getChildren().add(treeItem);
            }
        }

        // 监听事件
        dataTree.setOnMouseClicked(event -> {
            // 单机事件
            if (event.getClickCount() == 1) {
                dealSingleClick(dataTree);
            }
            if (event.getClickCount() == 2) {
                //dealDoubleClick();
            }
            //右击事件
            if (event.getButton() == MouseButton.SECONDARY) {
                System.out.println("123");
            }
        });

        dataInfo = new Pane();
        dataInfo.setId("data-info");
        dataInfo.setMinWidth(447);
        dataInfo.setMinHeight(427);
        dataTree.setRoot(rootTree);

        dataPane.getChildren().add(dataTree);
        dataPane.getChildren().add(dataInfo);
        HBox.setHgrow(dataTree, Priority.ALWAYS);
        HBox.setHgrow(dataInfo, Priority.ALWAYS);
        dataTab.setContent(dataPane);
        dataTab.setOnClosed(event -> dataTab = null);
    }


    /**
     * 处理单机事件
     */
    private static void dealSingleClick(TreeView<TreeNode> dataTree) {
        TreeItem<TreeNode> currItem = dataTree.getSelectionModel().getSelectedItem();
        TreeNode currNode = currItem.getValue();
        // 是否是双击的服务项
        if (!currNode.getType().equals(DATA_TREE_TYPE_D)) {
            return;
        }
        String key = currNode.getName();
        int index = (Integer) currNode.getData();
        DataNode dataNode = getDataNode(index, key);
        switch (dataNode.getType()) {
            case "string":
                dataInfo.getChildren().add(getTextView(dataNode));
        }
    }

    /**
     * 处理双击事件
     */
    private static void dealDoubleClick(TreeView<TreeNode> dataTree) {
        TreeItem<TreeNode> currItem = dataTree.getSelectionModel().getSelectedItem();
        TreeNode currNode = currItem.getValue();
        // 是否是双击的服务项
        if (!currNode.getType().equals(DATA_TREE_TYPE_S)) {
            return;
        }
        // 已经是打开状态
        if (!currItem.isLeaf()) {
            return;
        }
        Connect connect = (Connect) currNode.getData();
        // 打开连接
        String openMsg = openJedis(connect);
        if (!isNull(openMsg)) {
            FootWindow.changeFootState(false, openMsg);
        } else {
            FootWindow.changeFootState(true, "已连接到:".concat(connect.getText()));
        }
        // 获取数据库数据
        List<TreeNode> nodeList = getDbList();
        if (null != nodeList && !nodeList.isEmpty()) {
            for (TreeNode treeNode : nodeList) {
                TreeItem<TreeNode> treeItem = new TreeItem<>(treeNode);
                treeItem.setGraphic(getNodeIcon(LEFT_TREE_04));
                currItem.getChildren().add(treeItem);
            }
        }
        // 展开数据
        currItem.setExpanded(true);
    }


    private static Node getTextView(DataNode dataNode) {
        GridPane textPane = new GridPane();
        textPane.setId("text-view");
        textPane.setHgap(10);
        textPane.setPrefHeight(68);


        Label nameText = new Label("键名");
        TextField nameEdit = new TextField();
        nameEdit.setText(dataNode.getName());
        textPane.add(nameText, 0, 0);
        textPane.add(nameEdit, 1, 0);

        Label typeText = new Label("类型");
        TextField typeEdit = new TextField();
        typeEdit.setText(dataNode.getType());
        textPane.add(typeText, 2, 0);
        textPane.add(typeEdit, 3, 0);

        Label sizeText = new Label("大小");
        TextField sizeEdit = new TextField();
        sizeEdit.setText(dataNode.getSize());
        textPane.add(sizeText, 0, 1);
        textPane.add(sizeEdit, 1, 1);

        Label timeText = new Label("时效");
        TextField timeEdit = new TextField();
        sizeEdit.setText(dataNode.getTime());
        textPane.add(timeText, 2, 1);
        textPane.add(timeEdit, 3, 1);


        return textPane;
    }

}
