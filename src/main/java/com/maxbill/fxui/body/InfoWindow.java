package com.maxbill.fxui.body;

import com.maxbill.base.bean.InfoNode;
import com.maxbill.base.bean.LogsNode;
import com.maxbill.base.bean.UserNode;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static com.maxbill.fxui.body.BodyWindow.getTabPane;
import static com.maxbill.fxui.util.CommonConstant.TEXT_TAB_INFO;
import static com.maxbill.util.RedisUtil.*;

/**
 * 信息窗口
 *
 * @author MaxBill
 * @date 2019/07/10
 */
public class InfoWindow {

    private static Tab infoTab = null;

    public static void initInfoTab() {
        if (null == infoTab) {
            infoTab = new Tab(TEXT_TAB_INFO);
            getTabPane().getTabs().add(infoTab);
        }
        getTabPane().getSelectionModel().select(infoTab);

        HBox dataPane = new HBox();

        VBox lpane = new VBox();
        TableView lpaneTop = getLpaneTop();
        TableView lpaneEnd = getLpaneEnd();
        lpane.getChildren().add(lpaneTop);
        lpane.getChildren().add(lpaneEnd);

        TableView rpane = getRpaneInf();

        dataPane.getChildren().add(lpane);
        dataPane.getChildren().add(rpane);
        infoTab.setContent(dataPane);
        infoTab.setOnClosed(event -> infoTab = null);

        lpane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                lpaneTop.setPrefWidth(newValue.doubleValue());
                lpaneEnd.setPrefWidth(newValue.doubleValue());
            }
        });
        lpane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                lpaneTop.setPrefHeight(newValue.doubleValue() * 0.3);
                lpaneEnd.setPrefHeight(newValue.doubleValue() * 0.7);
            }
        });

        dataPane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                lpane.setPrefWidth(newValue.doubleValue() / 2);
                rpane.setPrefWidth(newValue.doubleValue() / 2);
            }
        });
        dataPane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (newValue != null) {
                lpane.setPrefHeight(newValue.doubleValue());
                rpane.setPrefHeight(newValue.doubleValue());
            }
        });
    }


    private static TableView getLpaneTop() {
        ObservableList<UserNode> data = FXCollections.observableArrayList(getUserList());
        TableView<UserNode> tableView = new TableView<>(data);
        TableColumn<UserNode, String> idCol = new TableColumn<>("ID");
        TableColumn<UserNode, String> dbCol = new TableColumn<>("库号");
        TableColumn<UserNode, String> hostCol = new TableColumn<>("主机");
        TableColumn<UserNode, String> timeCol = new TableColumn<>("时长");
        idCol.setSortable(false);
        dbCol.setSortable(false);
        hostCol.setSortable(false);
        timeCol.setSortable(false);
        tableView.getColumns().add(idCol);
        tableView.getColumns().add(hostCol);
        tableView.getColumns().add(timeCol);
        tableView.getColumns().add(dbCol);
        tableView.setItems(data);

        idCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((UserNode) node).getId());
        });
        dbCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((UserNode) node).getDb());
        });
        hostCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((UserNode) node).getHost());
        });
        timeCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((UserNode) node).getTime());
        });
        return tableView;
    }

    private static TableView getLpaneEnd() {
        ObservableList<LogsNode> data = FXCollections.observableArrayList(getLogsList());
        TableView<LogsNode> tableView = new TableView<>(data);
        TableColumn<LogsNode, String> logKeysCol = new TableColumn<>("ID");
        TableColumn<LogsNode, String> logTimeCol = new TableColumn<>("操作时间");
        TableColumn<LogsNode, String> exeTimeCol = new TableColumn<>("操作耗时");
        TableColumn<LogsNode, String> logArgsCol = new TableColumn<>("操作耗时");

        logKeysCol.setSortable(false);
        logTimeCol.setSortable(false);
        exeTimeCol.setSortable(false);
        logArgsCol.setSortable(false);

        tableView.getColumns().add(logKeysCol);
        tableView.getColumns().add(logTimeCol);
        tableView.getColumns().add(exeTimeCol);
        tableView.getColumns().add(logArgsCol);

        tableView.setItems(data);
        logKeysCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((LogsNode) node).getLogId());
        });
        logTimeCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((LogsNode) node).getLogTime());
        });
        exeTimeCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((LogsNode) node).getExeTime());
        });
        logArgsCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((LogsNode) node).getLogArgs());
        });
        return tableView;
    }


    private static TableView getRpaneInf() {
        ObservableList<InfoNode> data = FXCollections.observableArrayList(getInfoList());
        TableView<InfoNode> tableView = new TableView<>(data);
        TableColumn<InfoNode, String> keyCol = new TableColumn<>("项目");
        TableColumn<InfoNode, String> valCol = new TableColumn<>("数值");
        TableColumn<InfoNode, String> txtCol = new TableColumn<>("描述");
        tableView.getColumns().add(keyCol);
        tableView.getColumns().add(valCol);
        tableView.getColumns().add(txtCol);
        tableView.setItems(data);
        keyCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((InfoNode) node).getKey());
        });
        valCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((InfoNode) node).getVal());
        });
        txtCol.setCellValueFactory(param -> {
            Object node = ((TableColumn.CellDataFeatures) param).getValue();
            return new SimpleStringProperty(((InfoNode) node).getTxt());
        });
        return tableView;
    }


}
