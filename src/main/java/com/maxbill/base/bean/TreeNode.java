package com.maxbill.base.bean;

import com.maxbill.util.KeyUtil;
import lombok.Data;

/**
 * 树实体
 *
 * @author MaxBill
 * @date 2019/07/06
 */

@Data
public class TreeNode {

    private String id;

    private String name;

    /**
     * C:服务分类
     * S:服务连接
     * D:服务数据
     */
    private String type;

    private Object data;

    public TreeNode(String name) {
        this.id = KeyUtil.getKey();
        this.name = name;
        this.type = "C";
    }

    public TreeNode(Connect connect) {
        this.id = connect.getId();
        this.name = connect.getText();
        this.type = "S";
        this.data = connect;
    }

    public TreeNode(String name, Object data) {
        this.id = KeyUtil.getKey();
        this.name = name;
        this.type = "D";
        this.data = data;
    }

    @Override
    public String toString() {
        return name;
    }

}
